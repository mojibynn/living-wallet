package com.ascend.recombee.controllers;

import com.ascend.recombee.services.RecombeeService;
import com.recombee.api_client.bindings.Recommendation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by canapat on 8/7/2017 AD.
 */
@RestController
@RequestMapping("api/v1")
public class RecombeeController {

    private RecombeeService recombeeService;

    @Autowired
    public RecombeeController(RecombeeService recombeeService) {
        this.recombeeService = recombeeService;
    }

    @GetMapping("initdata")
    private void initData() {
        recombeeService.initData();
    }

    @GetMapping("get/userbase/{id}")
    private ResponseEntity<Recommendation[]> getUserbaseRecommendations(@PathVariable String id) {
        return new ResponseEntity<Recommendation[]>(recombeeService.getUserBaseReccommendations(id), HttpStatus.OK);
    }

    @PostMapping("get/userbase/{id}")
    private ResponseEntity<Recommendation[]> postGetUserbaseRecommendations(@PathVariable String id) {
        return new ResponseEntity<Recommendation[]>(recombeeService.getUserBaseReccommendations(id), HttpStatus.OK);
    }

    @GetMapping("get/itembase/{id}")
    private ResponseEntity<Recommendation[]> getItembaseRecommendations(@PathVariable String id) {
        return new ResponseEntity<Recommendation[]>(recombeeService.getItemBaseRecommendations(id), HttpStatus.OK);
    }

    @GetMapping("get/itembase/{itemID}/{userID}")
    private ResponseEntity<Recommendation[]> getItembaseRecommendationsOnUser(@PathVariable String itemID, @PathVariable String userID) {
        return new ResponseEntity<Recommendation[]>(recombeeService.getItemBaseRecommendationsOnUser(itemID, userID), HttpStatus.OK);
    }

}
