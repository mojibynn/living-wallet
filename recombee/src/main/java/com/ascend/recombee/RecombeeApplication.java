package com.ascend.recombee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecombeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecombeeApplication.class, args);
	}
}
