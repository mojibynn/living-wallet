package com.ascend.recombee.services;

import com.ascend.recombee.repositories.Recombee;
import com.recombee.api_client.bindings.Recommendation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by canapat on 8/7/2017 AD.
 */

@Service
public class RecombeeService {
//    private static Recombee recombee = new Recombee();
    private Recombee recombee;

    @Autowired
    public RecombeeService(Recombee recombee) {
        this.recombee = recombee;
    }

    public void initData() {
        recombee.createDataWithProperty();
    }

    public Recommendation[] getUserBaseReccommendations(String id) {
        return recombee.getUserBaseRecommendations(id);
    }

    public Recommendation[] getItemBaseRecommendations(String id) {
        return recombee.getItemBaseRecommendations(id);
    }

    public Recommendation[] getItemBaseRecommendationsOnUser(String itemID, String userID) {
        return recombee.getItemBaseRecommendations(itemID, userID);
    }
}
