package com.ascend.recombee.repositories;

import com.recombee.api_client.RecombeeClient;
import com.recombee.api_client.api_requests.*;
import com.recombee.api_client.bindings.Recommendation;
import com.recombee.api_client.exceptions.ApiException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by canapat on 8/7/2017 AD.
 */
@Repository
public class Recombee {
    public static boolean RETURN_PROPERTIES = true;
    public static String MIN_RELEVANCE = "medium";
    public static double ROTATION_RATE = 0.02;
    public static int COUNT = 5;
    public static String DATABASE_ID = "test-recommend-system";
    public static String TOKEN_ID = "w90CfapiLiZheXX3w3eGWX5Yg4oV2HjlKprespJZy5SnXgA8sWZsXswNFsOpwC3Y";
    public static RecombeeClient client = new RecombeeClient(DATABASE_ID, TOKEN_ID);

    public Recommendation[] getUserBaseRecommendations(String id) {
        Recommendation[] recommended = new Recommendation[0];
        try {
            // Get 5 recommendations for user 'user-25'
            recommended = client.send(new UserBasedRecommendation("user-"+id, COUNT)
                    .setReturnProperties(RETURN_PROPERTIES)
                    .setMinRelevance(MIN_RELEVANCE)
                    .setRotationRate(ROTATION_RATE));
            System.out.println("Recommended items:");
            for (Recommendation rec : recommended)
                System.out.println(rec.getId() + " " + rec.getValues().get("genres"));
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return recommended;
    }

    public Recommendation[] getItemBaseRecommendations(String id) {
        Recommendation[] recommended = new Recommendation[0];
        try {
            // Get 5 recommendations for user 'user-25'
            recommended = client.send(new ItemBasedRecommendation(id, COUNT)
                    .setReturnProperties(RETURN_PROPERTIES)
                    .setMinRelevance(MIN_RELEVANCE)
                    .setRotationRate(ROTATION_RATE));
            System.out.println("Recommended items:");
            for (Recommendation rec : recommended)
                System.out.println(rec.getId() + " " + rec.getValues().get("genres"));
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return recommended;
    }

    public Recommendation[] getItemBaseRecommendations(String itemID, String userID) {
        Recommendation[] recommended = new Recommendation[0];
        try {
            // Get 5 recommendations for user 'user-25'
            recommended = client.send(new ItemBasedRecommendation(itemID, COUNT)
                    .setTargetUserId(userID)
                    .setReturnProperties(RETURN_PROPERTIES)
                    .setMinRelevance(MIN_RELEVANCE)
                    .setRotationRate(ROTATION_RATE));
            System.out.println("Recommended items:");
            for (Recommendation rec : recommended)
                System.out.println(rec.getId() + " " + rec.getValues().get("genres"));
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return recommended;
    }

    public void createData() {
        System.out.println("Start Recommendation Test");
        try {
            client.send(new ResetDatabase());
            final int NUM = 100;
            // Generate some random purchases of items by users
            final double PROBABILITY_PURCHASED = 0.1;
            Random r = new Random();
            ArrayList<Request> addPurchaseRequests = new ArrayList<Request>();
            for (int i = 0; i < NUM; i++)
                for (int j = 0; j < NUM; j++)
                    if (r.nextDouble() < PROBABILITY_PURCHASED) {

                        AddPurchase request = new AddPurchase(String.format("user-%s", i), String.format("item-%s", j))
                                .setCascadeCreate(true); // Use cascadeCreate parameter to create
                        // the yet non-existing users and items
                        AddDetailView request1 = new AddDetailView(String.format("user-%s", i), String.format("item-%s", j))
                                .setCascadeCreate(true);
                        addPurchaseRequests.add(request);
                        addPurchaseRequests.add(request1);
                    }

            System.out.println("Send purchases");
            client.send(new Batch(addPurchaseRequests)); //Use Batch for faster processing of larger data

        } catch (ApiException e) {
            e.printStackTrace();
            //use fallback
        }
    }

    public void createDataWithProperty() {
        System.out.println("Start Recommendation Test");
        try {
            client.send(new ResetDatabase());
            // add user property
            client.send(new AddUserProperty("age", "int"));
            client.send(new AddUserProperty("job", "string"));

            // add item property
            client.send(new AddItemProperty("year", "int"));
            client.send(new AddItemProperty("genres", "set"));
//            client.send(new AddUserProperty("likes", "set"));

            ArrayList<Request> data = new ArrayList<Request>();

            // Add items
            data.add(new SetItemValues("item-1",
                    new HashMap<String, Object>() {{
                        put("year", 1994);
                        put("genres", new String[]{"Crime Fiction", "Drama", "Thriller"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-2",
                    new HashMap<String, Object>() {{
                        put("year", 1999);
                        put("genres", new String[]{"Science Fiction", "Action", "Adventure"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-3",
                    new HashMap<String, Object>() {{
                        put("year", 1999);
                        put("genres", new String[]{"Drama", "Existentialism"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-4",
                    new HashMap<String, Object>() {{
                        put("year", 2003);
                        put("genres", new String[]{"Adventure", "Fantasy", "Action"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-5",
                    new HashMap<String, Object>() {{
                        put("year", 2008);
                        put("genres", new String[]{"Superhero", "Drama", "Action", "Adventure", "Thriller", "Crime Fiction"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-6",
                    new HashMap<String, Object>() {{
                        put("year", 2014);
                        put("genres", new String[]{"Crime Fiction"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-7",
                    new HashMap<String, Object>() {{
                        put("year", 1950);
                        put("genres", new String[]{"Science Fiction", "Drama"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-8",
                    new HashMap<String, Object>() {{
                        put("year", 1993);
                        put("genres", new String[]{"Existentialism"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-9",
                    new HashMap<String, Object>() {{
                        put("year", 1993);
                        put("genres", new String[]{"Action"});
                    }}
            ).setCascadeCreate(true));

            data.add(new SetItemValues("item-10",
                    new HashMap<String, Object>() {{
                        put("year", 2005);
                        put("genres", new String[]{"Superhero", "Adventure", "Thriller", "Crime Fiction"});
                    }}
            ).setCascadeCreate(true));

            // Add user
            data.add(new SetUserValues("user-1",
                    new HashMap<String, Object>() {{
                        put("age", 18);
                        put("job", "student");
                    }}
            ).setCascadeCreate(true));

            data.add(new SetUserValues("user-2",
                    new HashMap<String, Object>() {{
                        put("age", 23);
                        put("job", "software engineer");
                    }}
            ).setCascadeCreate(true));

            data.add(new SetUserValues("user-3",
                    new HashMap<String, Object>() {{
                        put("age", 23);
                        put("job", "software engineer");
                    }}
            ).setCascadeCreate(true));

            // Add to purchase
            data.add(new AddPurchase("user-1", "item-1")
                    .setCascadeCreate(true));

            data.add(new AddPurchase("user-1", "item-3")
                    .setCascadeCreate(true));

            data.add(new AddPurchase("user-2", "item-1")
                    .setCascadeCreate(true));

            data.add(new AddPurchase("user-2", "item-4")
                    .setCascadeCreate(true));

            data.add(new AddPurchase("user-3", "item-5")
                    .setCascadeCreate(true));

            data.add(new AddPurchase("user-3", "item-9")
                    .setCascadeCreate(true));

            data.add(new AddPurchase("user-3", "item-10")
                    .setCascadeCreate(true));

            // Add to bookmark
            data.add(new AddBookmark("user-1", "item-4")
                    .setCascadeCreate(true));

            data.add(new AddBookmark("user-2", "item-1")
                    .setCascadeCreate(true));

            data.add(new AddBookmark("user-3", "item-10")
                    .setCascadeCreate(true));


            System.out.println("Send Data");
            client.send(new Batch(data)); //Use Batch for faster processing of larger data

        } catch (ApiException e) {
            e.printStackTrace();
            //use fallback
        }
    }
}
